// - #2:
const string = '>>v^<vvv>^^<><^v<<<v<>><>vv><vv<><v><^>^^<^<v<><<^v^>^>v>^^>>v>>>^<<>>v<>vv>>^<vvv<><vvv><^>^<^<v<^<v><<<<><>^>v<v<<<><^<^^vv<v^^<<vv^^<^vv^v>><<><<><v^v<<<v<><^^v^vv<<v<^><v<>v>^<<><>v<>^^v><<^v^><^^v>^<>v><>^<<<>vv>v^><>>v>^^^v^<<<<v^<^>>v<^>>v>>v^vvvv><<^>^^^<^>^^>v<>>v<^^v^^><>v<><v<<<<<<<>>vv^>v<><<><^<<v^>>^<<v<>^vvv<^^<vvv><>>v<vvv^<>v<v>v>^^^><v^^vv^><<<>v<v^v>>v>^^><>v<v<<>>v>^^v<>>>v>>v^<<><>v>v><>^vv><v^<v^>>^><vv<<>^<>v^<^<^vv<v<>>v>>>>^<<<>>>><v^><v>v^v<^^<>v><^v<^vv<^v><^^vv^vv>^>^v><<>^<^^vvv<^v^>>>v><>v<<^^<<>^><<<v^^^<>v>^v^^>^^><<v>^>v^<^>^vv<v<>vv^^^<>v>^<>v<<^v<>vv<<^^<>vv><v<>>vv>v<v^<v>>>vv^><^v^v>>^vv><^^<v<vv^>>>>v^v>^v<<>^<<^v^v^<^>vvv^^<<<<<v<v<v^^>^><^v<v>^^^v<<<^><<v<^<vvv^^>>^^^^^v>^vvv^^<><<>v>^^<<>v>v^<<<<v^<^v<^><<<^>><>v<v<<vv<^>v>^v<v<vv<v<<^>^<v>>^><>>>v<<v>>^<^>>v<<^v^^v><<v>><^^>>^vv<v^>^vv^^^<<^<>><<v<<<v>><>>><<^^^^vvv^^>>vv>v^<>v^>vvv<>^>v^>vv>^^^^<v^>^>v>vvv<v<>^^^^^>v^>^v^v<<<vv^<<><>^v>>^vv^<^v>>v>v<><^>><><^>v<><^^>^v>>v<v^^<>^vv<vv>v^<v<>>>^>>v>^^^^><<v>vv<^v<^>^>>v>^^><^v<<<v>>v>v>><>^^>^v<<>v<^v<v<^^><v>v^>>><^<<^<v<vv<>><^><^><v<v>vv>^^^v^<v>>v><<<>>v^v<v<v^v^>>^v>>^<^>vv^<vv^>v<<<v^><^^v^>v^><<>>^^^<^<^^<><vvv<^>^^>v^^><<>^vv^^<^^v^^^>^^<>v<>^<>>v>vv<>^>^>^<v<v><^v>v^v>^>>v<<v<^^<^<>^<vv><vv><^^^vv<^><<v>>vv>v^<><><>>^><<<vv<><>v^><^>>^><<v>vvv^<^<v^vv<^^<^vv<<v>^<^>^^v<>^<>><^^^>><>^v>>^^vvv<^vv^>^>v^<v^v^v>>v><^><>>^^>v^^^^vv^v>v<><><>vvvv<>^v>vvv>^^^>>vv^<v<v>^v<^<^v<^v<>v>^v^^vvvv<^^^v<^vvvv^>^vvv>v>v^^>>v^^<vv^<^^<<^v>><^>^>>v>v<v<>^^>>><^>>vv>v>v>>v<v<<^>><v<><>>v><>^v>v<><<^v<<v>^>^^v><<><^>^>^<^>^v>^<>v>v^^v>^^>>>^vv^><<<<^<v^>>>^>v^v>v<v^^<>>v<v<^><<<<v<>>vv^<v>^>vv<^v>^<v^^vvv>^^^<^^v>v^v^vv^^^v^^<v><<<^><v>^v^>>>^>^^v<>>v^^<v^<v>><^>^<<<>v<v<^<^>^vvv<>>v^<>>^>^<>v^v<v<<v^v<^>v^<<<v>><v^<v>>><>v<<v<<^<v^^><v><v^<<<v<v<<<>v<^v<v<><^v^v<><^>v>>^v^<>v>v^>>>';
const steps = string.split('');
let i = 0;
let j = 0;
let counter = 0;

var map = new Map();
map.set(i + 'x' + j, '1')

for (let n = 0; n < steps.length; n++) {
    if (steps[n] === '^') i++;
    else if (steps[n] === 'v') i--;
    else if (steps[n] === '>') j++;
    else j--;

    if (map.get(i + 'x' + j) === undefined) {
        map.set(i + 'x' + j, '1');
    }
    else {
        map.set(i + 'x' + j, 'n')
    }
}

for (var value of map.values()) {
    if (value === 'n') counter++;
}

console.log(counter);